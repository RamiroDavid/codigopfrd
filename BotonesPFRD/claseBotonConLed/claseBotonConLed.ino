//#define boton 2
//#define led 4

class Boton
{
  public:
    Boton(int pinBoton, int pinDondeActuar); //constructor por parametros
    //Boton();
    bool state();
    efectoBoton();
    //  int static _contador;
    int _pinBoton;
    int _pinDondeActuar;
    bool static _state;

    //int static _tiempoUmbral;
    //long static _tiempoComienzo;

  private:

};

Boton::Boton(int pinBoton, int pinDondeActuar)
{
  _pinBoton = pinBoton;
  pinMode(_pinBoton, INPUT); //poner como INPUT_PULLUP para interrupcion
  _pinDondeActuar = pinDondeActuar;
  pinMode(_pinDondeActuar, OUTPUT);
  //  _contador = 0;
  //  _tiempoUmbral = 200;
  // _tiempoComienzo = 0;

}
/*//Constructor por defecto de Boton
  Boton::Boton() //No deberia usar un constructor por defecto
  {
  pinMode(pinBoton, INPUT);
  _state = LOW;
  _pinBoton = 0;

  }
*/

bool Boton::state()
{
  return digitalRead(_pinBoton);

}

Boton::efectoBoton()
{
  //Efecto de boton segun que boton este apretado. Al ser virtual puedo definir una para cada objeto instanciado de Boton
  /*
    if (millis() - _tiempoComienzo > _tiempoUmbral)
    {
      _tiempoComienzo = millis();

    _state = !_state; */
  bool state = digitalRead(_pinBoton);
  digitalWrite(_pinDondeActuar, state);
  //Serial.print("presione boton veces:");
  //Serial.print("%d", _contador);
  //_contador++;
}


Boton boton(2, 4); //Creo objeto boton de clase Boton
const int tiempoUmbral = 200;
long tiempoComienzo = 0;

void setup() {
  // put your setup code here, to run once:

}

void loop() {
  // put your main code here, to run repeatedly:
  
  if (boton.state() == HIGH) {
    if (millis() - tiempoComienzo > tiempoUmbral){
    boton.efectoBoton();
      } else boton.efectoBoton();  
    tiempoComienzo = millis();
   }

}
