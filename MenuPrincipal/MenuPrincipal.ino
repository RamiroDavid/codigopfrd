/*
 * 
 * Código proyecto final 
 * Ingeniería Biomédica - Universidad Favaloro.

 * Titulo: Dispositivo para el control de la presion en cuffs endotraqueales.
 * Alumno: Ramiro David
 * Directores:
 * -Ing. Federico G. Roux
 * -Lic. Sergio Di Yelsi
 */
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <LiquidMenu.h>
#include <SensorPresion.h>
#include <Motor.h>
#include <Valvula.h>
#include "BotonLongPress.h"


#define NRO_VALORES_PROMEDIO 45


LiquidCrystal_I2C lcd(0x27, 16, 2);

// Instanciado de botones
const bool pullup = true;

BotonLongPress hold(7, pullup);
BotonLongPress up(8, pullup);
BotonLongPress down(4, pullup);
BotonLongPress enter(10, pullup);
BotonLongPress bloqueo(A1, pullup);
BotonLongPress alarma(A2, pullup);
BotonLongPress desinflado(A0, pullup);

//Instancio motor
Motor motor(2,3);
//Instancio valvula
Valvula valvula(5,6);
//Instancio sensor
SensorPresion sensor(0x28);

//Variables
int valor = 0;
float valorLeido = 0;
float presion_por_defecto = 25;
int margen = 1;
boolean flagHold = false; // Indica si esta en modo Hold o no
boolean flagCambiarPresion = true; // Impide que se cambie la presion cuando se esta en modo Hold
boolean flagBloqueo = false; // Indica si esta o no bloqueado el dispositivo
boolean flagEstadoControlPresion = false; //Indica si esta o no activado el control de presion en un primer instante. Usuario mantiene apretado enter para activar este flag

char* lista_unidades[] = {"cmH2O", "mBar", "hPa"};
char* unidad = lista_unidades[0]; //Unidad por defecto
float unidad_presion_identidad = 1; //Valor por defecto ya que cmH20 es unidad de lectura del sensor
int cont;
int valores_presion_por_defecto[] = {22,23,24,25,26,27,28};
int valores_t_hold[] = {1,4,5,6,7,8,9};
float suma_valores_presion_leidos = 0;

unsigned long tiempo_hold = 300000; //valor por defecto. Son 5 min.
unsigned long comienzo_tiempo_hold = 0;

int valor_presion_compensada = presion_por_defecto; //No multiplico por unidad_presion_identidad pq por defecto vale 1
float presion_sobrepresion = 0;

float presion_sincronizada=0;

float valores_leidos[]= {0,0,0};
int contador = 0;
int variable_config = 0;

int n=0;
int aux = 0;
float resultado_p = 0;
float resultado_i = 0;
float valor_pwm_motor = 0;

float vector_presion_sincronizada[]={0,0,0,0,0};
int contadorFlagPrimeraVuelta = 0;
float acumulador_presiones = 0;
boolean flagPrimeraVuelta = true;
float presion_sincronizada_promedio = 0;
boolean flagAuxFueraDeRango = false;

boolean flagAlarma = false;
boolean flagPanelControl = false;

/////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////ZONA DEFINICION FUNCIONES//////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

int funcionAux(){
  suma_valores_presion_leidos = 0;
  for(cont = 0; cont < NRO_VALORES_PROMEDIO; cont++){
    suma_valores_presion_leidos = suma_valores_presion_leidos + (sensor.LeerSensor() * unidad_presion_identidad);
    Serial.print(sensor.LeerSensor());
    Serial.print("\n");
  }
  Serial.print("\n");
  Serial.print(presion_sincronizada);
  Serial.print("\n");
  Serial.print("\n");
  presion_sincronizada = suma_valores_presion_leidos/NRO_VALORES_PROMEDIO;
  
if( flagEstadoControlPresion == true){
  

 if( flagPrimeraVuelta == true)
 {
  presion_sincronizada_promedio = presion_sincronizada;
  acumulador_presiones = acumulador_presiones + presion_sincronizada;
  vector_presion_sincronizada[contadorFlagPrimeraVuelta] = presion_sincronizada;
  contadorFlagPrimeraVuelta++;
  if ( contadorFlagPrimeraVuelta == 5){
    flagPrimeraVuelta = false;
  }


 } else {
  
          acumulador_presiones = acumulador_presiones - vector_presion_sincronizada[aux];
          aux ++;
          vector_presion_sincronizada[aux-1] = presion_sincronizada;
          acumulador_presiones = acumulador_presiones + vector_presion_sincronizada[aux-1];

          if(presion_sincronizada < 2){
            flagAlarma = true;
          }
          
          if (aux == 5){
          aux = 0;
          }
          presion_sincronizada_promedio = acumulador_presiones/5;
        }
}

  return presion_sincronizada_promedio;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////// ZONA MENU////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

//Menu principal
LiquidLine presion_objetivo_line(0, 0, "Pobj:   ", valor_presion_compensada);
LiquidLine presion_leida_line(0, 1, "Pleida: ", funcionAux);
LiquidLine unidades_line(11, 0, unidad);
LiquidScreen presion_screen(presion_objetivo_line, presion_leida_line, unidades_line);

LiquidMenu menu(lcd, presion_screen, 1 );

//Menu de configuracion
LiquidLine config_line1(1, 0, "Unidades");
LiquidLine config_line2(0, 1, "y P por defecto");
LiquidScreen config_screen1(config_line1, config_line2);



LiquidLine config_line3(1, 0, "T hold y");
LiquidLine config_line4(0, 1, "valor sobrepres.");
LiquidScreen config_screen2(config_line3, config_line4);

LiquidMenu menu_configuracion(lcd, config_screen1, config_screen2);

LiquidSystem menu_system(menu, menu_configuracion);

//////////////////////////////////////////////////////////////////////////////////////
/////////////////////////ZONA FUNCIONES 2/////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////


// Function to be attached to the presion_line object.
void subir_presion() {
  if (valor_presion_compensada < 32 * unidad_presion_identidad) {
    valor_presion_compensada += 1 * unidad_presion_identidad;
  } else {
    valor_presion_compensada = 32 * unidad_presion_identidad;
  }
  menu_system.softUpdate();
}

// Function to be attached to the presion_line object.
void bajar_presion() {
  if (valor_presion_compensada > 20 * unidad_presion_identidad) {
    valor_presion_compensada -= 1 * unidad_presion_identidad;
  } else {
    valor_presion_compensada = 20 * unidad_presion_identidad;
  }
  menu_system.softUpdate();
}

void aumentarPresionHold(){
  valor_presion_compensada = valor_presion_compensada + presion_sobrepresion;
  lcd.clear();
  lcd.print("Aumento p hold");
  delay (1000);
  menu_system.update();
  flagHold = true;
  flagCambiarPresion = false;
  comienzo_tiempo_hold = millis();
}

void decrementarPresionHold(){
  valor_presion_compensada = valor_presion_compensada - 5 * unidad_presion_identidad;
  lcd.clear();
  lcd.print("Cancelar p hold");
  delay (1000);
  menu_system.update();
  flagHold = false;
  flagCambiarPresion = true;
}

void go_back() {
  // This function takes reference to the wanted menu.
  menu_system.change_menu(menu);
}

void cambiar_unidades() {
  variable_config=0;
  lcd.clear();
  do {
    lcd.print(lista_unidades[variable_config]);
    if (up.checkButton() == 1){
      variable_config++;;
      if (variable_config == 3) variable_config=0;
    }
    if (down.checkButton() == 1){
      variable_config--;
      if (variable_config <= 0) variable_config=2;
    }
    delay(100);
    lcd.clear();
    }
  while(enter.checkButton() != 2);
  unidad = lista_unidades[variable_config];

  valor_presion_compensada = valor_presion_compensada / unidad_presion_identidad;
  
  if ( variable_config == 0){
    unidad_presion_identidad = 1;
  }
   if ( variable_config == 1){
    unidad_presion_identidad = 0.981;
  }

   if ( variable_config == 2){
    unidad_presion_identidad = 0.981;
  }

  valor_presion_compensada = valor_presion_compensada * unidad_presion_identidad;

  menu_system.update();
  cambiar_presion_por_defecto();
}

void cambiar_presion_por_defecto(){
  lcd.clear();
  variable_config = 3;
    do {
      lcd.setCursor(10,0);
      lcd.print(unidad);
      lcd.setCursor(0,0);
    lcd.print(valores_presion_por_defecto[variable_config] * unidad_presion_identidad);
    if (up.checkButton() == 1){
      variable_config++;
      if (variable_config > 6) variable_config=6;
    }
    if (down.checkButton() == 1){
      variable_config--;
      if (variable_config < 0) variable_config=0;
    }
    delay(100);
    lcd.clear();
    }
  while(enter.checkButton() != 2);
  presion_por_defecto = valores_presion_por_defecto[variable_config];

  valor_presion_compensada = valor_presion_compensada + (presion_por_defecto * unidad_presion_identidad - valor_presion_compensada);
  
  menu_system.update();
}

void cambiar_Thold(){
variable_config = 2;

lcd.clear();
  do {
    lcd.setCursor(10,0);
      lcd.print("min");
      lcd.setCursor(0,0);
    lcd.print(valores_t_hold[variable_config]);
    if (up.checkButton() == 1){
      variable_config++;;
      if (variable_config > 6) variable_config = 6;
    }
    if (down.checkButton() == 1){
      variable_config--;
      if (variable_config <0 ) variable_config = 0;
    }
    delay(100);
    lcd.clear();
    }
  while(enter.checkButton() != 2);
  tiempo_hold = valores_t_hold[variable_config];
  tiempo_hold = tiempo_hold * 60 * 1000; //Tiempo de hold en milisegundos
  

  menu_system.update();
  cambiar_valor_sobrepresion();
}

void cambiar_valor_sobrepresion(){
  lcd.clear();
  variable_config = 3;

    do {
      lcd.setCursor(10,0);
      lcd.print(unidad);
      lcd.setCursor(0,0);
    lcd.print((valores_presion_por_defecto[variable_config] - 20) * unidad_presion_identidad);
    if (up.checkButton() == 1){
      variable_config++;
      if (variable_config > 5) variable_config=5;
    }
    if (down.checkButton() == 1){
      variable_config--;
      if (variable_config < 1) variable_config=1;
    }
    delay(100);
    lcd.clear();
    }
  while(enter.checkButton() != 2);
  presion_sobrepresion = (valores_presion_por_defecto[variable_config] - 20) * unidad_presion_identidad ;

  menu_system.update();
  
}


void apagarAlarma(){
  noTone(12);
  flagAlarma = false;
  
}


void setup() {
  Serial.begin(250000);

 // Inicializacion LCD
  lcd.init();
  lcd.backlight();

 //Inicializar motor
  motor.init();
  
  //Inicializar valvula
  valvula.init();
 
  //Inicializar sensor
  sensor.init();

    // Inicializacion Menu.
  menu.init();

  pinMode(12, OUTPUT);
  digitalWrite(12, LOW);





  // Function to attach functions to LiquidLine objects.
  presion_objetivo_line.attach_function(1, subir_presion);
  presion_objetivo_line.attach_function(2, bajar_presion);
  
  config_line1.attach_function(4, cambiar_unidades);

  config_line3.attach_function(4, cambiar_Thold);

  menu_system.update(); 
  Serial.print("Presion Leida\t");
     Serial.print("Valor pwm\n");

}

void goto_menu_configuracion() {
  menu_system.change_menu(menu_configuracion);
}


void loop() {
if (flagHold == true){
 if ( comienzo_tiempo_hold + tiempo_hold - millis() > 4294957186 ){
   decrementarPresionHold();
 }
}

if (flagAlarma == true){
  tone(12, 1000);
}


if (bloqueo.checkButton() == 3){
  if( flagBloqueo == true ) {
    flagBloqueo = false;
    lcd.clear();
    menu_system.update();
  } else {
            flagBloqueo = true;
            lcd.setCursor(15,1);
            lcd.print("b");
         }
}

int aux = alarma.checkButton();
if (aux == 3){
  delay(100);
  if (hold.checkButton() == 3) {
    if(flagBloqueo == false){ 
    goto_menu_configuracion();
    }
  } else apagarAlarma();  //Hago algo por alarma sonando
}

if (flagBloqueo != true) // Todo esto se ejecuta si no esta el equipo bloqueado. Funciona solamente alarma y desbloqueo y control de la presion cuando el equipo esta bloqueado
{
    aux=enter.checkButton();
    if (aux != false){
        if (aux == 3 && flagHold == false) {
          flagEstadoControlPresion = true;
        }else if (aux == 1) {
                if(menu_system.get_currentScreen() == &config_screen1){
                        cambiar_unidades();
                      }else if(menu_system.get_currentScreen() == &config_screen2){
                        cambiar_Thold();
                      }else menu_system.switch_focus();
              }   
    }  
    
    
    if (up.checkButton() != false ) {
      if (menu_system.get_currentScreen() == &presion_screen && flagCambiarPresion == true) {
         subir_presion();
      }else menu_system.previous_screen();
    }
    
    if (down.checkButton() != false ) {
      if (menu_system.get_currentScreen() == &presion_screen  && flagCambiarPresion == true) {
        bajar_presion();
      }else menu_system.next_screen();
    }

aux=hold.checkButton();
if (aux != false){
  
  if (aux == 3){
    delay(100);
    if (alarma.checkButton() == 3){
        if (flagPanelControl == false){
            //Entro panel configuracion
            goto_menu_configuracion();
            flagPanelControl = true;
        } else{
          menu_system.change_menu(menu);
          flagPanelControl = false;
        }
    } else if (flagHold != true){
            aumentarPresionHold();
           } else decrementarPresionHold();           
  }     
}

if(desinflado.checkButton() == 3){
  flagEstadoControlPresion = false;
  valvula.activarValvula(100,false,0);
  flagAlarma = false;
  delay(10000);
  valvula.desactivarValvula();
  
}

}

/////////////////////////////Control de la presion/////////////////////////////
if(menu_system.get_currentScreen() == &presion_screen && flagEstadoControlPresion == true){


  if (presion_sincronizada != valor_presion_compensada)
  {

    
    // Algoritmo de control Proporcional Integral
    if(n == 3){
      n = 0;
    }
    //Agrego un valor a la cadena de 3 mediciones
    valores_leidos[n] = presion_sincronizada;
    
    
    //Hago Pobjetivo - Pleido
    resultado_p = valor_presion_compensada - valores_leidos[n];
    
    //Sumo cada una de esas Pobjetivo - Pleido
    switch (n) {
      case 0:
        resultado_i=resultado_p + valores_leidos[1] + valores_leidos[2]; 
        break;
      case 1:
        resultado_i=resultado_p + valores_leidos[0] + valores_leidos[2];     
        break;
      case 2:
        resultado_i=resultado_p + valores_leidos[0] + valores_leidos[1];
        break;
    }
    
    // Valor pwm = 1.9* resultado_p + 0.17 * resultado_i
    valor_pwm_motor= 1.9 * resultado_p + 0.17 * resultado_i;
    
    
    if(valor_pwm_motor > 60) valor_pwm_motor = 60;
    if(valor_pwm_motor < 30) valor_pwm_motor = 30;
    
    if(presion_sincronizada <= valor_presion_compensada - (7 * margen)){
      
      motor.activarMotor(valor_pwm_motor);            
      valvula.activarValvula(20, true, 5); // activarValvula(int porcentaje, progresivo true o false, paso de escalon)
      //Prende progresivo valvula para evitar picos de presion
      valvula.activarValvula(40,false, 0);
      delay(20);     
      valvula.desactivarValvula();
      delay(10);
      motor.desactivarMotor();
          
    }
    if(presion_sincronizada > valor_presion_compensada - (margen * 7) && presion_sincronizada < valor_presion_compensada){

      motor.activarMotor(valor_pwm_motor);      
      valvula.activarValvula(18,false,0);
      delay(30);      
      motor.desactivarMotor();
      valvula.activarValvula(23, true, 3);//Prende progresivo valvula para evitar picos de presion
      valvula.desactivarValvula();
      
    }

    if(presion_sincronizada >= valor_presion_compensada && presion_sincronizada < valor_presion_compensada + margen) {
      valvula.desactivarValvula();
    }
    
    if( presion_sincronizada >= valor_presion_compensada + margen){
     valvula.activarValvula(26,false,0);
     delay(10);
     valvula.desactivarValvula();
    }

    //rutina de control valores sobrepico

    if( presion_sincronizada >= valor_presion_compensada + 5 * margen){
     valvula.activarValvula(40,false,0);
     delay(30);
     valvula.desactivarValvula();
    }

    if (flagHold == true){
      Serial.print(comienzo_tiempo_hold + tiempo_hold - millis()); 
    }
 
    n++;
    
    }

    
menu_system.softUpdate();

if( presion_sincronizada < 10){
  lcd.setCursor(9, 1);
  lcd.print(" ");
}


}


}
