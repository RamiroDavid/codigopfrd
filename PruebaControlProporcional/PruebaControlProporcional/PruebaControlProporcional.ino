int TiempoMuestreo = 100;       // tiempo de muestreo Se encuentra en milisegundos
unsigned long pasado = 0;     // tiempo pasado (Se hace para asegurar tiempo de muestreo)
unsigned long ahora;


/*-------------------------------------------------------------------
VALORES PID
-------------------------------------------------------------------*/
//Referencia, también llamado SetPoint
float Ref = 255;
//Salida, también llamada Process Variable ó PV
double Y;
//Error
double error;
//Señal de control
double U;
//Ganancia proporcional
int Kp = 2;
/*-------------------------------------------------------------------
FIN VALORES PID
-------------------------------------------------------------------*/

void setup()
{
  Serial.begin(9600);
  //pinLed(pinLed, OUTPUT);
}
void loop()
{
  //Obtenemos algún valor de un sensor, en este caso,
  //una fotoresistencia, el resultado es Y
  valorLDR = analogRead(pinLDR);
  valorLDR = constrain(valorLDR, min, max);
  valorLDR = map(valorLDR, min, max, 255, 0);  
  Y = valorLDR;

  // tiempo actual
  ahora = millis();
  // diferencia de tiempo actual- pasado
  int CambioTiempo = ahora - pasado;
  // si se supera el tiempo de muestreo
  if (CambioTiempo >= TiempoMuestreo)
  {
    //Ejecución de código
    error = Ref - Y;
    Serial.print("error = Ref - Y: ");
    Serial.print(error);
    Serial.print(" = ");
    Serial.print(Ref);
    Serial.print(" - ");
    Serial.println(Y);

    U = Kp * error;
    Serial.print("U = Kp * error: ");
    Serial.print(U);
    Serial.print(" = ");
    Serial.print(Kp);
    Serial.print(" * ");
    Serial.println(error);

    if (U < -255)     // límites de saturación de la señal de control
    {
      U = -255;
    }
    if (U > 255)    // límites de saturación de la señal de control
    {
      U = 255;
    }
    //Actualizamos el valor del ledValue
    U = map(U, 255, 0, 0, 255);
    analogWrite(pinLed, U);
    Serial.println(U);
    Serial.println("");

    // actualizar tiempo
    pasado = ahora;
  }
}
