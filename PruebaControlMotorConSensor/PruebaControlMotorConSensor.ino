 #include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include<SensorPresion.h>
#include <Motor.h>
#include "BotonLongPress.h"

const bool pullup = true;
BotonLongPress left(A0, pullup);
BotonLongPress hold(7, pullup);
BotonLongPress up(8, pullup);
BotonLongPress down(11, pullup);
BotonLongPress enter(10, pullup);


//Instancio motor
Motor motor(2,3);
//Instancio sensor
SensorPresion sensor(0x28);
//Crear el objeto lcd  dirección  0x27 y 16 columnas x 2 filas
LiquidCrystal_I2C lcd(0x27,16,2);  //

int valor = 0;
int valorPresion = 25;
int margen = 2;

void setup() {

  // Inicializar el LCD
  lcd.init();
  
    //Encender la luz de fondo.
  lcd.backlight();
  
  Serial.begin(9600);
  
 //Inicializar motor
  motor.init();
 
  //Inicializar sensor
  sensor.init();
  

  
  // Escribimos el Mensaje en el LCD.
  lcd.print("Presion balon:");
  motor.activarMotor(40);
}

void loop() {
   // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)
  lcd.setCursor(0, 1);
   // Escribimos el número de segundos trascurridos

 //Envio valores a display y a archivo ValoresPresion.txt
  valor = sensor.LeerSensor();


  
  lcd.print(valor);
  Serial.println(valor);
  motor.activarMotor(100);
  delay(2000);
  motor.desactivarMotor();
  delay(2000);

 
  
}
