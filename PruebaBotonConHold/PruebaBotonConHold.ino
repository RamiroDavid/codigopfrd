#include "BotonLongPress.h"
#define buttonPin 10        // analog input pin to use as a digital input


BotonLongPress boton(10);

void setup() {

    Serial.begin(9600);   
    // Set button input pin
   pinMode(buttonPin, INPUT);
   digitalWrite(buttonPin, HIGH );
   
}

void loop() {
   // Get button event and act accordingly
   int b = boton.checkButton();
   if (b == 1) boton.clickEvent();
   if (b == 2) boton.doubleClickEvent();
   if (b == 3) boton.holdEvent();
}
