
//Deberia funcionar. Creo que es pq la rutina de ISR no debe usar la funcion millis()
#include <Boton.h>

#define pinBoton 2

int contador = 0;

Boton boton(pinBoton);

void setup()
{
  boton.init();
  Serial.begin(9600);
  attachInterrupt(digitalPinToInterrupt(pinBoton), funcionAuxiliar, FALLING);
  
}

void loop()
{
  
  if (contador != boton._contador)
  {
    contador = boton._contador;
    Serial.println(contador);
  }
 
}

void funcionAuxiliar()
{
  boton.chequeoBoton();

}
