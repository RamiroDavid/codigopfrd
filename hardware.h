/*
* Proyecto Final: Dispositivo para el control automático 
* de presión en cuffs endotraqueales.
* Alumno: Ramiro David
* Universidad Favaloro.
* 
* Conexión pin a pin para placa Arduino UNO.
*/
#ifndef hardware_h
#define hardware_h
 
#include "Arduino.h"
 
class hardware
{
  public:

//Direcciones I2C
int direccionLCD = (int)0x27;
int direccionSensor = (int)0x28;

//Carriles de tension de protoBoard
char pin3V = "TopLine";
char pin5V = "LowLine";

//Pines LCD con I2C
char pinGNDLCD = "gnd";
char pinVccLCD = "5V";
int pinSDALCD = A4;
int pinSCLLCD = A5;

//Pines sensor presion
char pin1SensorPresion = "gnd"; 
char pin2SensorPresion = "vdd"; 
char pin3SensorPresion ="NC"; 
char pin4SensorPresion ="NC"; 
int pin5SensorPresion = A4; //  SDA
int pin6SensorPresion = A5; // SCK

//Pines botones
int pinBotonEnter = 10;
int pinBotonUp = 8;
int pinBotonDown = 4;
int pinBotonHold = 7;
int pinBotonBloqueo = A1;
int pinBotonDesinflar = A0;
int pinBotonAlarma = A2;


//Pines driver motores
//Motor compresor/vacio
int pin1Motor = 2;
int pin2Motor = 3;
int pinPWMmotor = 9;

//Válvula conmutadora
int pin1Valvula = 5;
int pin2Valvula = 6;
int pinPWMvalvula = 11;

//Buzzer
int pinBuzzer = 12;



};
 
#endif