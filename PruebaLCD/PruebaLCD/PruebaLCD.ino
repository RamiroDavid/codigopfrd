#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include<SensorPresion.h>


//Instancio sensor
SensorPresion Sensor(0x28);
//Crear el objeto lcd  dirección  0x27 y 16 columnas x 2 filas
LiquidCrystal_I2C lcd(0x27,16,2);  //

int valor = 0;
int valorPresion = 25;
int margen = 2;

void setup() {
  // Inicializar el LCD
  lcd.init();
 
  //Inicializar sensor
  Sensor.init();
  
  //Encender la luz de fondo.
  lcd.backlight();
  
  // Escribimos el Mensaje en el LCD.
  lcd.print("Presion balon:");
}

void loop() {
   // Ubicamos el cursor en la primera posición(columna:0) de la segunda línea(fila:1)
  lcd.setCursor(0, 1);
   // Escribimos el número de segundos trascurridos
   
  valor = Sensor.LeerSensor();
    lcd.print(valor);
 
  lcd.print(" cmH2O");

  delay(100);
}
