/* Modulacion de ancho de pulso. El usuario elige valor de fondo de escala desdeado(Del 0 al 100) y se ve evidenciado en el voltaje del pin PWM.
Se creo un archivo .h y uno .cpp creando la clase PWM
Se usaron funciones nativas analogRead y analogWrite.

*/

#include <PWM.h>

//Define conexiones de placa: Generar archivo .h y meto todos los defines ahi y luego lo incluyo en el main. Al menos con entradas y saidas digitales

int valorPorcentaje;

PWM pwm(11 , valorPorcentaje); // (pinSalidaPWM, ValorPorcentaje deseado


int valorObtenido = valorObtenido;

void setup()
{
  Serial.begin(9600);
  Serial.println("Ingresar valor de porcentaje de operacion: ");
}
 
void loop()
{
 
  if (Serial.available() == true)
  {
    valorObtenido = Serial.parseInt(); //Lee proximo int en puerto serie
    Serial.println(valorObtenido);
  
    pwm.asignarValorPorcentaje(valorObtenido);
    pwm.modular();
    
  }else return;

}
